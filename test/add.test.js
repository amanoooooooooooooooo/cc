// add.test.js
var {
    add, mul,
    minus
} = require('../src/add.js');
var expect = require('chai').expect;

describe('加法函数的测试', function () {
    it('1 加 1 应该等于 2', function () {
        expect(add(1, 1)).to.be.equal(2);
    });
    it('任何数加0等于自身', function () {
        expect(add(1, 0)).to.be.equal(1);
        expect(add(0, 0)).to.be.equal(0);
    });
});
describe('乘法函数的测试', function () {
    it('1 乘 1 应该等于 1', function () {
        expect(mul(1, 1)).to.be.equal(1);
    });
});
describe('减法函数的测试', function () {
    it('1 - 1 应该等于 0', function () {
        expect(minus(1, 1)).to.be.equal(0);
    });
});
